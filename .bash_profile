# To the extent possible under law, the author(s) have dedicated all 
# copyright and related and neighboring rights to this software to the 
# public domain worldwide. This software is distributed without any warranty. 
# You should have received a copy of the CC0 Public Domain Dedication along 
# with this software. 
# If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 

# ~/.bash_profile: executed by bash(1) for login shells.

# The copy in your home directory (~/.bash_profile) is yours, please
# feel free to customise it to create a shell
# environment to your liking.  If you feel a change
# would be benifitial to all, please feel free to send
# a patch to the msys2 mailing list.

# User dependent .bash_profile file
#export HOME=/home

# source the users bashrc if it exists
if [ -f "${HOME}/.bashrc" ] ; then
  source "${HOME}/.bashrc"
fi

# Set PATH so it includes user's private bin if it exists
#if [ -d "${HOME}/bin" ] ; then
#   PATH="/opt/bin:/mingw64/bin:/~/bin:/opt/tools/arm/bin:${PATH}"
#fi
echo "---------------------------------------------------------------------------------------------------------"
echo $PWD 
export MSYS_BASE_PATH=`cygpath -m /`
export MSYS_DRIVE=/w
export MSYS_PATH="$MSYS_DRIVE/msys2"
export ARDUINO_VERSION="NEW_VERSION"
export USER_HOME="/home/$USER"
export PATH="$USER_HOME/bin:/opt/openocd/bin:/opt/bin:/opt/tools/stm8:/mingw64/bin:/home/bin:/opt/tools/arm-st/bin:${PATH}"
export SKETCHBOOK=$MSYS_DRIVE/arduino/$ARDUINO_VERSION/portable/sketchbook
export STM32_TOOLCHAIN_PATH=/opt/tools/arm/bin
export STM32_PLATFORM_PATH=$MSYS_DRIVE/arduino/$ARDUINO_VERSION/portable/packages/STM32/hardware/stm32/1.9.0
export EICON_VARIANTS_PATH=$MSYS_DRIVE/arduino/$ARDUINO_VERSION/portable/packages/Eicon/hardware/stm32/1.0.1
export ARDUINO_SDK_PATH=$USER_HOME/arduino/$ARDUINO_VERSION
export STM32Cube_PATH=/opt/tools/STM32Cube
export STM32CubeIDE_PATH=/opt/ide/STM32CubeIDE
export STM32_TOOLCHAIN_PATH=/opt/tools/arm/bin
export GCC_SUFFIX=.exe
echo "---------------------------------------------------------------------------------------------------------"
echo "MSYS_BASE_PATH                           = $MSYS_BASE_PATH"
echo "Home                                     = $HOME"
echo "MSYS_DRIVE                               = $MSYS_DRIVE"
echo "ARDUINO_SDK_PATH                         = $ARDUINO_SDK_PATH"
echo "SKETCHBOOK                               = $SKETCHBOOK"
echo "STM32_TOOLCHAIN_PATH                     = $STM32_TOOLCHAIN_PATH"
echo "STM32_PLATFORM_PATH                      = $STM32_PLATFORM_PATH"
echo "EICON_VARIANTS_PATH                      = $EICON_VARIANTS_PATH"
echo " "
echo "Cmdline Commands Software"
echo "Create Empty Gitlab and Local Repo       = createblankproject 'NameOfProject' "
echo "Create Stm32 Gitlab and Local Repo       = BlinkStm32Project 'Stm32_Blink_F427ZI' 'xx' "
echo "Create DinRduino Gitlab and Local Repo   = BlinkDinRDuinoProject 'DRD_Blink_F103CB' 'xB' "
echo "Cmdline Commands Hardware"
echo "Create Cadint DRD Gitlab and Local Repo  = Create_Cadint_DinRDuino_Repo BB PRJ_NAME VERSION    # BaseBoards"
echo "Create Cadint DRD Gitlab and Local Repo  = Create_Cadint_DinRDuino_Repo CB PRJ_NAME VERSION    # CpuBoards"
echo "Create Cadint DRD Gitlab and Local Repo  = Create_Cadint_DinRDuino_Repo RB PRJ_NAME VERSION    # RiserBoards" 
echo " "
echo "hello Msys264 is at your service"
echo " "
echo "---------------------------------------------------------------------------------------------------------"

# Set MANPATH so it includes users' private man if it exists
# if [ -d "${HOME}/man" ]; then
#   MANPATH="${HOME}/man:${MANPATH}"
# fi

# Set INFOPATH so it includes users' private info if it exists
# if [ -d "${HOME}/info" ]; then
#   INFOPATH="${HOME}/info:${INFOPATH}"
# fi
