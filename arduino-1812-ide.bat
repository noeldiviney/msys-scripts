echo off
REM Portable launcher for msys64
REM 1  check for valid command line argument
REM 2  copy .bash_profile_%1 to home\%username%\.bash_profile
REM 3  launch bash shell in the desired home directory ie msys2_shel.cmd -shell "bash" -where "path"
echo ----------------------------------------------------------
echo "arg1 = %1%"
echo "cwd = %cd%"
echo "user = %username%"
echo $CWD
echo ----------------------------------------------------------
echo "copy preferences_%1.txt   %cd%\home\%username%\arduino-1.8.12\portable\preferences.txt"	
copy preferences_%1.txt     %cd%\home\%username%\arduino-1.8.12\portable\preferences.txt	
%cd%\home\%username%\arduino-1.8.12\arduino.exe