﻿#--------------------------------------------------------------
#
#    Launching the Msys2 in a fully configured way.
#
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        helper_function_1        # Calling helper_function_1
#
#        helper_function_2        # Calling helper_function_2
#    }
#
#    #---------------------------------------------------------
#    # Helper Function 1
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Helper Function 2
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something else
#    }
#
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
param([string]$VERSION = "VERSION", [string]$SKETCH = "SKETCH") ;

$MSYS2_PATH = "W:\msys2"
$ARDUINO_PATH = "W:\arduino\$VERSION"
$SKETCH_PATH= "$ARDUINO_PATH\portable\sketchbook\arduino"
$USER=(Split-Path -Path $env:USERPROFILE -Leaf)
$PROFILE_PATH = "$MSYS2_PATH\home\$USER"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)  Entering               main";

    echo "Line $(CurrentLine)  Calling                echo_args"
    echo_args        # Calling echo_args

    echo "Line $(CurrentLine)  Returning from         echo_args"

    echo "Line $(CurrentLine)  Install                .bash_profile template"
    Copy-Item w:\arduino\msc\.bash_profile $PROFILE_PATH\.bash_profile

    echo "Line $(CurrentLine)  Calling                Edit_Profile"
    Edit_Profile

    echo "Line $(CurrentLine)  Calling                Launch_Msys2"
    Launch_Msys2

#    helper_function_2        # Calling helper_function_2
}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
#$PREFS_PATH = "W:\"
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering               echo_args"
    Write-Host -NoNewline "Line $(CurrentLine)  Arguments              ";
    Write-Host -NoNewline "VERSION = $VERSION, "
    Write-Host -NoNewLine "SKETCH = $SKETCH, "
    Write-Host "BOARD = $BOARD"
    Write-Host "Line $(CurrentLine)  MSYS2_PATH           = $MSYS2_PATH";            
    Write-Host "Line $(CurrentLine)  PROFILE_PATH         = $PROFILE_PATH";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH          = $SKETCH_PATH";            
    Write-Host "Line $(CurrentLine)  Leaving                echo_args"
    Write-Host "Line $(CurrentLine)  USER                 = $USER";
}

#---------------------------------------------------------
# Edit Profile
#---------------------------------------------------------
function Edit_Profile
{
    Write-Host "Line $(CurrentLine)  Entering               Edit_Profile"

#    Write-Host "Line $(CurrentLine)  Editing                .bass_profile and  replace NEW_BOARD with $BOARD"
#    ((Get-Content -path $PREFS_PATH\preferences.txt -Raw) -replace 'NEW_BOARD', $BOARD) | Set-Content -Path $PREFS_PATH\preferences.txt

    Write-Host "Line $(CurrentLine) Editing                .bash_profile and  replace NEW_VERSION with $VERSION"
    ((Get-Content -path $PROFILE_PATH\.bash_profile -Raw) -replace 'NEW_VERSION', $VERSION) | Set-Content -Path $PROFILE_PATH\.bash_profile

#    Write-Host "Line $(CurrentLine) Editing                preferences.txt and  replace NEW_SKETCH with $SKETCH"
#    ((Get-Content -path $PREFS_PATH\preferences.txt -Raw) -replace 'NEW_SKETCH', $SKETCH) | Set-Content -Path $PREFS_PATH\preferences.txt

    Write-Host "Line $(CurrentLine) Leaving                Edit_Profile"
}

#---------------------------------------------------------
# Launch Msys2
#---------------------------------------------------------
function Launch_Msys2
{
    Write-Host "Line $(CurrentLine) Entering               Launch_Msys2"

    Write-Host "Line $(CurrentLine) Executing              $MSYS2_PATH\msys2_shell.cmd -shell bash -where $SKETCH_PATH\$SKETCH\build "
#    & $ARDUINO_PATH\arduino.exe
    &  $MSYS2_PATH\msys2_shell.cmd -shell bash -where $SKETCH_PATH\$SKETCH\build

    Write-Host "Line $(CurrentLine) Leaving                Launch_Arduino_IDE"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
main
