﻿#--------------------------------------------------------------
#
#    Launching the Arduino IDE in a fully configured way.
#
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        helper_function_1        # Calling helper_function_1
#
#        helper_function_2        # Calling helper_function_2
#    }
#
#    #---------------------------------------------------------
#    # Helper Function 1
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Helper Function 2
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something else
#    }
#
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
param([string]$VERSION = "VERSION", [string]$SKETCH = "SKETCH", [string]$BOARD = "BOARD") ;

$MSYS2_PATH = "W:\msys2"
$ARDUINO_PATH = "W:\arduino\$VERSION"
$PREFS_PATH = "$ARDUINO_PATH\portable"
$SKETCH_PATH= "$PREFS+PATH\sketchbook\arduino"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)  Entering               main";

    echo "Line $(CurrentLine)  Calling                echo_args"
    echo_args        # Calling echo_args

    echo "Line $(CurrentLine)  Returning from         echo_args"

    echo "Line $(CurrentLine)  Install                preferences.txt remplate"
    Copy-Item preferences.txt W:\arduino\1.8.12\portable\preferences.txt

    echo "Line $(CurrentLine)  Calling                Edit_Prefs"
    Edit_Prefs

    echo "Line $(CurrentLine)  Calling                Launch_Arduino_IDE"
    Launch_Arduino_IDE

#    helper_function_2        # Calling helper_function_2
}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
#$PREFS_PATH = "W:\"
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering               echo_args"
    Write-Host -NoNewline "Line $(CurrentLine)  Arguments              ";
    Write-Host -NoNewline "VERSION = $VERSION, "
    Write-Host -NoNewLine "SKETCH = $SKETCH, "
    Write-Host "BOARD = $BOARD"
    Write-Host "Line $(CurrentLine)  MSYS2_PATH           = $MSYS2_PATH";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH           = $PREFS_PATH";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH          = $SKETCH_PATH";            
    Write-Host "Line $(CurrentLine)  Leaving                echo_args"
}

#---------------------------------------------------------
# Edit Prefs
#---------------------------------------------------------
function Edit_Prefs
{
    Write-Host "Line $(CurrentLine)  Entering               echo_args"

    Write-Host "Line $(CurrentLine)  Editing                preferences.txt and  replace NEW_BOARD with $BOARD"
    ((Get-Content -path $PREFS_PATH\preferences.txt -Raw) -replace 'NEW_BOARD', $BOARD) | Set-Content -Path $PREFS_PATH\preferences.txt

    Write-Host "Line $(CurrentLine) Editing                preferences.txt and  replace NEW_VERSION with $VERSION"
    ((Get-Content -path $PREFS_PATH\preferences.txt -Raw) -replace 'NEW_VERSION', $VERSION) | Set-Content -Path $PREFS_PATH\preferences.txt

    Write-Host "Line $(CurrentLine) Editing                preferences.txt and  replace NEW_SKETCH with $SKETCH"
    ((Get-Content -path $PREFS_PATH\preferences.txt -Raw) -replace 'NEW_SKETCH', $SKETCH) | Set-Content -Path $PREFS_PATH\preferences.txt

    Write-Host "Line $(CurrentLine) Leaving                echo_args"
}

#---------------------------------------------------------
# Edit Prefs
#---------------------------------------------------------
function Launch_Arduino_IDE
{
    Write-Host "Line $(CurrentLine) Entering               Launch_Arduino_IDE"

    Write-Host "Line $(CurrentLine) Executing              $ARDUINO_PATH\arduino.exe "
    & $ARDUINO_PATH\arduino.exe

    Write-Host "Line $(CurrentLine) Leaving                Launch_Arduino_IDE"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
main
